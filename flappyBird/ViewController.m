//
//  ViewController.m
//  flappyBird
//
//  Created by Clicklabs 104 on 12/9/15.
//  Copyright © 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UIButton *playButton;
@end

@implementation ViewController
@synthesize playButton;
- (void)viewDidLoad {
    [super viewDidLoad];
    HighScore=[[NSUserDefaults standardUserDefaults]integerForKey:@"highScore"];
    scoreLabel.text=[NSString stringWithFormat:@"High Score %li",(long)HighScore];
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)playBtnPress:(id)sender {
    [self performSegueWithIdentifier:@"nextScreen" sender:self];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
