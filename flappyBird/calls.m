//
//  calls.m
//  flappyBird
//
//  Created by Clicklabs 104 on 12/9/15.
//  Copyright © 2015 vijay kumar. All rights reserved.
//

#import "calls.h"
int selectNumber;
NSString *dial;
@interface calls ()
@property (strong, nonatomic) IBOutlet UILabel *dialLabel;
@property (strong, nonatomic) IBOutlet UIButton *one;
@property (strong, nonatomic) IBOutlet UIButton *two;
@property (strong, nonatomic) IBOutlet UIButton *three;
@property (strong, nonatomic) IBOutlet UIButton *four;
@property (strong, nonatomic) IBOutlet UIButton *five;
@property (strong, nonatomic) IBOutlet UIButton *six;
@property (strong, nonatomic) IBOutlet UIButton *seven;
@property (strong, nonatomic) IBOutlet UIButton *eight;
@property (strong, nonatomic) IBOutlet UIButton *nine;
@property (strong, nonatomic) IBOutlet UIButton *star;
@property (strong, nonatomic) IBOutlet UIButton *zero;
@property (strong, nonatomic) IBOutlet UIButton *hashButton;
@property (strong, nonatomic) IBOutlet UIButton *clearBtn;
@property (strong, nonatomic) IBOutlet UIButton *simOne;

@end

@implementation calls
@synthesize one;
@synthesize two;
@synthesize three;
@synthesize four;
@synthesize five;
@synthesize six;
@synthesize seven;
@synthesize eight;
@synthesize nine;
@synthesize zero;
@synthesize star;
@synthesize hashButton;
@synthesize clearBtn;
@synthesize dialLabel;
@synthesize simOne;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    dialLabel=[[NSUserDefaults standardUserDefaults]objectForKey:@"dialLabel"];
//    starString=[NSString stringWithFormat:@"*"];
   
}
- (IBAction)oneBtn:(id)sender {
//    selectNumber=selectNumber *10;
//    selectNumber=selectNumber+1;
//    dialLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    dialLabel.text=[NSString stringWithFormat:@"%@1",dialLabel.text];
}
- (IBAction)twoBtn:(id)sender {
//    selectNumber=selectNumber *10;
//    selectNumber=selectNumber+2;
//    dialLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    dialLabel.text=[NSString stringWithFormat:@"%@2",dialLabel.text];
}
- (IBAction)threeBtn:(id)sender {
//    selectNumber=selectNumber *10;
//    selectNumber=selectNumber+3;
//    dialLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    dialLabel.text=[NSString stringWithFormat:@"%@3",dialLabel.text];
}
- (IBAction)fourBtn:(id)sender {
//    selectNumber=selectNumber *10;
//    selectNumber=selectNumber+4;
//    dialLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    dialLabel.text=[NSString stringWithFormat:@"%@4",dialLabel.text];
}
- (IBAction)fiveBtn:(id)sender {
//    selectNumber=selectNumber *10;
//    selectNumber=selectNumber+5;
//    dialLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    dialLabel.text=[NSString stringWithFormat:@"%@5",dialLabel.text];
}
- (IBAction)sixBtn:(id)sender {
//    selectNumber=selectNumber *10;
//    selectNumber=selectNumber+6;
//    dialLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    dialLabel.text=[NSString stringWithFormat:@"%@6",dialLabel.text];
}
- (IBAction)sevenBtn:(id)sender {
//    selectNumber=selectNumber *10;
//    selectNumber=selectNumber+7;
//    dialLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    dialLabel.text=[NSString stringWithFormat:@"%@7",dialLabel.text];
}
- (IBAction)eightBtn:(id)sender {
//    selectNumber=selectNumber *10;
//    selectNumber=selectNumber+8;
//    dialLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    dialLabel.text=[NSString stringWithFormat:@"%@8",dialLabel.text];
}
- (IBAction)nineBtn:(id)sender {
//    selectNumber=selectNumber *10;
//    selectNumber=selectNumber+9;
//    dialLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    dialLabel.text=[NSString stringWithFormat:@"%@9",dialLabel.text];
}
- (IBAction)starBtn:(id)sender {
   
    
//    dialLabel.text=[NSString stringWithFormat:@"%@",starString];
//    selectNumber=selectNumber+1;
//    dialLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    dialLabel.text=[NSString stringWithFormat:@"%@*",dialLabel.text];
}
- (IBAction)zeroBtn:(id)sender {
//    selectNumber=selectNumber *10;
//    selectNumber=selectNumber+0;
//    dialLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    dialLabel.text=[NSString stringWithFormat:@"%@0",dialLabel.text];

}
- (IBAction)hashBtn:(id)sender {
//    selectNumber=selectNumber *10;
//    selectNumber=selectNumber+1;
//    dialLabel.text=[NSString stringWithFormat:@"%i",selectNumber];
    dialLabel.text=[NSString stringWithFormat:@"%@*",dialLabel.text];

}
- (IBAction)clearBtnPressed:(id)sender {
//    selectNumber=0;
    dialLabel.text=@"";
    
}
- (IBAction)simBtnPressed:(id)sender {
    dial=dialLabel.text;
    [[NSUserDefaults standardUserDefaults]setObject:dial forKey:@"dialLabel"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    NSLog(@"%@",dial);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
