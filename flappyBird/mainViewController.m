//
//  mainViewController.m
//  flappyBird
//
//  Created by Clicklabs 104 on 12/10/15.
//  Copyright © 2015 vijay kumar. All rights reserved.
//

#import "mainViewController.h"

@interface mainViewController ()

@end

@implementation mainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)nextScreen:(id)sender {
    [self performSegueWithIdentifier:@"next" sender:self];
}
- (IBAction)callsBtn:(id)sender {
      [self performSegueWithIdentifier:@"calls" sender:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
