//
//  game.m
//  flappyBird
//
//  Created by Clicklabs 104 on 12/9/15.
//  Copyright © 2015 vijay kumar. All rights reserved.
//

#import "game.h"
//#import "ViewController.h"

int birdFlight;
int randomTopTunnelPosition;
int randomBottomTunnelPosition;
int score;
NSInteger highScore;
NSTimer *birdMovement;
NSTimer *tunnelMovement;
@interface game ()
@property (strong, nonatomic) IBOutlet UILabel *score1;
@property (strong, nonatomic) IBOutlet UIButton *startGame;
@property (strong, nonatomic) IBOutlet UIButton *exit;
@property (strong, nonatomic) IBOutlet UIImageView *bird;
@property (strong, nonatomic) IBOutlet UIImageView *tunnelTop;
@property (strong, nonatomic) IBOutlet UIImageView *tunnelBottom;
@property (strong, nonatomic) IBOutlet UIImageView *top;
@property (strong, nonatomic) IBOutlet UIImageView *bottom;

@end

@implementation game
@synthesize score1;
@synthesize startGame;
@synthesize exit;
@synthesize bird;
@synthesize tunnelTop;
@synthesize tunnelBottom;
@synthesize top;
@synthesize bottom;
- (void)viewDidLoad {
    [super viewDidLoad];
    tunnelTop.hidden=YES;
    tunnelBottom.hidden=YES;
    exit.hidden=YES;
    top.hidden=YES;
    bottom.hidden=YES;
    score1.hidden=YES;
    score=0;
    highScore=[[NSUserDefaults standardUserDefaults]integerForKey:@"highScore"];
    
}
-(void)birdMoving
{
    bird.center=CGPointMake(bird.center.x, bird.center.y-birdFlight);
    birdFlight=birdFlight-5;
    if (birdFlight<-20) {
        birdFlight=-20;
    }
    
    
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    birdFlight=18;
    
}
-(void)tunnelMoving
{
    tunnelTop.center=CGPointMake(tunnelTop.center.x-1, tunnelTop.center.y);
    tunnelBottom.center=CGPointMake(tunnelTop.center.x-1, tunnelBottom.center.y);
    if (tunnelTop.center.x<-35) {
       [self placeTunnel];
    }
    if (tunnelTop.center.x==38) {
        [self score];
    }
    if (CGRectIntersectsRect(bird.frame, tunnelTop.frame)) {
        [self gameOver];
    }
    if (CGRectIntersectsRect(bird.frame, tunnelBottom.frame)) {
        [self gameOver];
    }
    if (CGRectIntersectsRect(bird.frame, top.frame)) {
        [self gameOver];
    }
    if (CGRectIntersectsRect(bird.frame, bottom.frame)) {
        [self gameOver];
    }
}
-(void)placeTunnel
{
    randomTopTunnelPosition=arc4random() %380;
    randomTopTunnelPosition=randomTopTunnelPosition-258;
    randomBottomTunnelPosition=randomBottomTunnelPosition+ 750;
    tunnelTop.center=CGPointMake(250, randomTopTunnelPosition);
    tunnelBottom.center=CGPointMake(400, randomBottomTunnelPosition);
    
}
-(void)score
{
    score=score+1;
    score1.text=[NSString stringWithFormat:@"%i",score];
}
-(void)gameOver
{
    if (score>highScore) {
        [[NSUserDefaults standardUserDefaults]setInteger:score forKey:@"HighScore"];
    }
    [birdMovement invalidate];
    [tunnelMovement invalidate];
    exit.hidden=NO;
    tunnelTop.hidden=YES;
    tunnelBottom.hidden=YES;
    bird.hidden=YES;
    startGame.hidden=YES;
}
- (IBAction)starting:(id)sender {
    tunnelTop.hidden=NO;
    tunnelBottom.hidden=NO;
    top.hidden=NO;
    bottom.hidden=NO;
    startGame.hidden=YES;
    score1.hidden=NO;
    bird.hidden=NO;
    exit.hidden=YES;
    birdMovement=[NSTimer scheduledTimerWithTimeInterval:0.04 target:self selector:@selector(birdMoving) userInfo:nil repeats:YES];
    
    [self placeTunnel];
    
    tunnelMovement=[NSTimer scheduledTimerWithTimeInterval:-0.30 target:self selector:@selector(tunnelMoving) userInfo:nil repeats:YES];
    
}
- (IBAction)exitBtnPressed:(id)sender {
    [self performSegueWithIdentifier:@"previous" sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
