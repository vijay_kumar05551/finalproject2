//
//  main.m
//  flappyBird
//
//  Created by Clicklabs 104 on 12/9/15.
//  Copyright © 2015 vijay kumar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
