//
//  AppDelegate.h
//  flappyBird
//
//  Created by Clicklabs 104 on 12/9/15.
//  Copyright © 2015 vijay kumar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

